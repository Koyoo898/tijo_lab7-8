package pl.edu.pwsztar.domain.dto;

import java.util.List;

public class FileDto {

   private List<MovieDto> movieDtoList;
   public FileDto(List<MovieDto> movieDtoList){ this.movieDtoList = movieDtoList; }
   public List<MovieDto> getMovieDtoList(){ return  movieDtoList; }
}
