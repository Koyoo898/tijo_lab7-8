package pl.edu.pwsztar.service.serviceImpl;


import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.files.FileGenerator;
import pl.edu.pwsztar.domain.dto.MovieDto;

import java.io.*;
import java.util.Comparator;
import java.util.List;

@Service(value = "fileGenerator")
public class FileGeneratorImpl implements FileGenerator {
    @Override
    public InputStreamResource toTxt(FileDto fileDto) throws IOException{
        FileDto prepFileDto = sortByYear(fileDto);
        File file = File.createTempFile("tmp",".txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

        for (MovieDto movieDto : prepFileDto.getMovieDtoList()){
            bufferedWriter.write(movieDto.getYear() + " " + movieDto.getTitle());
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        fileOutputStream.flush();
        fileOutputStream.close();

        InputStream inputStream = new FileInputStream(file);
        return new InputStreamResource(inputStream);
    }

    private FileDto sortByYear(FileDto fileDto){
        List<MovieDto> movieDtoList = fileDto.getMovieDtoList();
        movieDtoList.sort(Comparator.comparing(MovieDto::getYear).reversed());
        return new FileDto(movieDtoList);
    }
}
